# ~/ABRÜPT/MARTA NACHT/EXTRAVAGANCE/*

La [page de ce livre](https://abrupt.ch/marta-nacht/extravagance/) sur le réseau.

## Sur le livre

Le patriarcat place le joug, mais la femme autonome est une extravagance, et toute extravagance peut devenir la révolution qui construit une ontologie nouvelle. Le renversement des valeurs passe par la dysfonction des corps, par l’autonomie du sujet-femme qui a la puissance de transformer le rapport du sujet au réel.

## Sur l'autrice

Berlin, naissance avec la mort de l’Est. Et l’errance en dehors de soi, à la poursuite des idées qui durent plus longtemps que toute culture.

## Sur la licence

Cet [antilivre](https://abrupt.ch/antilivre/) est disponible sous licence Creative Commons Attribution – Pas d’Utilisation Commerciale – Partage dans les Mêmes Conditions 4.0 International (CC-BY-NC-SA 4.0).

## Etc.

Vous pouvez également découvrir notre [site](https://abrupt.ch) pour davantage d'informations sur notre démarche, notamment quant au [partage de nos textes](https://abrupt.ch/partage/).
