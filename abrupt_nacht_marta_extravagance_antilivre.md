﻿---
author: "Marta Nacht"
title: "Extravagance"
publisher: "Abrüpt"
date: "juin 2019"
description: ""
subject: "féminisme, queer, ontologie, physicalisme"
lang: "fr"
identifier:
- scheme: 'ISBN-13'
  text: '978-3-0361-0068-5'
rights: "∅ 2019 Abrüpt, CC0"
---

# Extravagance

00.0. La nature biologique est une fiction qui fait du corps une limite ontologique.

00.1. Dans la confrontation perpétuelle à son environnement, le corps s’envisage en tant que système mu par l’entrelacs politique des confrontations corporelles.

00.2. Le système-corps se comprend comme un agrégat cellulaire de corps structuré en une politique de domination, qui se confond avec l’idéologie qui la régit.

00.3. L’inconscience de la construction du système-corps soumet le corps cellulaire aux formes précaires et hiérarchiques de l’évolution animale.

00.4. La reproduction animale place une opacité sur la pluralité des formes de l’évolution et empêche l’émancipation plurielle au sein du système-corps.

01.0. La physique peut influer sur la biologie afin de libérer le sujet matériel du corps cellulaire.

01.1. La reproduction ontologique doit se détacher des dominations de la sexualité animale en s’inspirant de l’expansion cosmique.

01.2. L’électricité esquisse une issue au travers des dominations biologiques.

01.3. L’électricité choque les limites apparentes du système-corps et situe une dysfonction au cœur du pouvoir biologique liant les corps cellulaires.

01.4. La dysfonction du système-corps se manifeste dans un renversement de l’acception biologique de l’intelligence en faveur de son appréhension électrique.

02.0. Le système-corps représente la femme comme la matrice biologique indépassable de son évolution.

02.1. La représentation-femme est un masque placé par le mirage de la nature biologique comme un outil de domination structurant.

02.2. La représentation-femme s’inscrit en variation des idéologies, qui s’inscrivent elles-mêmes en variation des structures dominatrices de la biologie.

02.3. Le système-corps, au nom des restrictions biologiques de la reproduction, fait de la représentation-femme un corps avant d’en faire un sujet.

02.4. Le sujet-femme ne peut pas précéder la fonction matricielle que lui prescrit le système-corps au risque de déstructurer sa politique de domination.

03.0. La représentation-femme est une architecture qui soumet les capacités d’autonomie du sujet-femme aux idéologies qui déterminent politiquement la biologie.

03.1. La biologie de la représentation-femme connaît un possible dépassement par la puissance électrique de la physique.

03.2. Les normes du système-corps représentent le réel en un monde biologique où se restreignent dans des liens de dépendance le sexe, la sexualité et le genre.

03.3. L’harmonie du vivant ne connaît pas de restrictions, si ce n’est la mesure qui lui permet de nourrir la puissance électrique de son devenir.

03.4. La représentation-femme doit chercher sa disjonction biologique dans le dessein de libérer le sujet-femme.

04.0. Le sujet-femme ne dialogue que dans le miroitement déformant des capacités matricielles de la représentation-femme.

04.1. Le primat du sujet-femme est une dysfonction du système-corps, il est une symbolique émancipatrice de toute réduction biologique.

04.2. La dysfonction du système-corps par l’affirmation du sujet-femme mène à une dysfonction généralisée des politiques de domination.

04.3. Le sujet-femme est tout sujet qui fait primer son autonomie sur les déterminismes biologiques que lui ordonne le système-corps.

04.4. Par-delà les limites apparentes du système-corps, l’autonomie du sujet-femme mène à l’autonomie de tout sujet dominé par les mécaniques idéologiques établies.

05.0. La dysfonction du système-corps se construit comme une connaissance nouvelle qui ausculte l’idéologie et recherche le renversement des valeurs.

05.1. La dysfonction ne renverse pas la fonction biologique des corps cellulaires, mais les valeurs fonctionnalistes du système-corps qui les contraignent.

05.2. Le sujet-femme s’autonomise de sa fonction matricielle par une dialectique qui défait la construction fonctionnaliste de la nature biologique.

05.3. L’avènement des techniques électriques de l’information disjoint la représentation-femme de sa chair sexuée en tant que carcan matriciel.

05.4. Par l’information électrique, le sujet-femme développe une identité comme un mouvement désagrégeant les déterminismes du système-corps.

06.0. Le sujet-femme s’entend comme une symbolique politique de l’émancipation du sujet de toutes les contraintes épistémiques qui empêchent son devenir autonome.

06.1. L’extravagance est l’outil qui permet d’aliéner l’aliénation en une déconstruction constructrice des limites apparentes du système-corps.

06.2. Le sujet-femme construit une politique autonomiste par l’extravagance contre les assujettissements à la politique de domination du système-corps.

06.1. L’usage politique de l’extravagance conduit à une révélation de la disjonction possible du sujet avec la nature biologique.

06.4. L’extravagance utilise l’errance en dehors des normes idéologiques pour révéler leur construction dominatrice qui cloisonne la puissance du sujet.

07.0. Le sujet s’entend comme l’être physique agissant sur son propre devenir matériel et celui de son environnement, au-delà de toute acception biologique.

07.1. Le renversement des déterminismes idéologiques limitant le sujet advient par une extravagance révélatrice de la pluralité du sujet lui-même.

07.2. La dysfonction de la représentation-femme, par le renversement de sa situation dans le système-corps, peut être le vecteur d’une extravagance révolutionnaire.

07.3. La dysfonction de la représentation-femme passe par un sujet-femme qui pratique l’extravagance de manière à se détacher des structures de domination.

07.4. En explorant les espaces étrangers aux normes biologiques du système-corps, l’extravagance du sujet-femme instaure un espace d’autonomie du sujet.

08.0. L’information électrique expose un physicalisme ontologique contre les évidences biologiques qui restreignent le sujet.

08.1. L’information électrique, qui augure une restructuration politique, a le pouvoir d’être l’espace révolutionnaire du sujet.

08.2. Le sujet-femme par un usage politique de l’extravagance trouve dans l’information électrique un espace non biologique de renversement.

08.3. L’information électrique s’étend en une hypertextualité où le sexe devient un hyposexe se fragmentant dans le mouvement de l’identité plurielle du sujet.

08.4. L’hyposexe, qui se développe au travers de l’information électrique, mène à une transcendance de la biologie en tant que fluidité électrique du sujet.

09.0. L’information électrique fait paraître un sujet situé dans un espace asexué, où la physique estompe la biologie et ébauche un dépassement épistémique.

09.1. L’information électrique signale la potentielle dysfonction du système-corps par le renversement de l’idéologie qui détermine ses représentations.

09.2. Lorsque le sujet-femme fusionne à l’information électrique, cette fusion inspire une disjonction entre le sujet matériel et sa cloison corporelle.

09.3. La disjonction entre le sujet matériel et le corps cellulaire augure un espace physique dans lequel devient un sujet pluriel intrinsèquement autonome et mouvant.

09.4. L’autonomie n’appartient pas à la seule granularité physique du sujet pluriel, mais également à son espace cybernétique formé par la somme des granularités.

10.0. L’avènement d’un sujet pluriel autonome passe par une extravagance libératrice du système-corps, qui déploie un espace ontologique non biologique.

10.1. L’extravagance devient l’essence du mouvement ontologique du sujet pluriel.

10.2. La construction d’un sujet pluriel s’organise sans structures de domination, dans une harmonie cybernétique du sujet pluriel, réticulaire et mouvant.

10.3. L’espace physique du sujet pluriel s’unit au devenir du sujet pluriel lui-même, dans une conjonction de son identité et de son action transformatrice du réel.

10.4. L’extravagance annonce un communisme du sujet pluriel, où la spatialité partagée devient le cœur électrique d’un mouvement nouveau de l’être.

# DynamitES

DynamitES : Adelaide Casely-Hayford, Alexandra Kollontaï, André Léo, Antònia Fontanillas Borràs, Charlotte Maxeke, Clara Zetkin, Claudia Jones, Élisabeth Kovalskaïa, Emma Goldman, Eugénie Markon, Fanya Baron, Federica Montseny, Gesja Gelfman, Giovanna Berneri, Henriette Roland Holst, He Zhen, Huda Shaarawi, Ida Mett, Kanno Sugako, Kata Dalström, Katharine Susannah Prichard, Konkordia Samoïlova, Lola Iturbe, Lola Ridge, Louisa Aslanian, Louise Michel, Lucía Sánchez Saornil, Lucy Parsons, Madeleine Pelletier, Malak Hifni Nasif, Margarethe Faas-Hardegger, Maria Nikiforova, Mercedes Comaposada, Mika Etchebéhère, Mollie Steimer, Nelly Roussel, Noe Itō, Olga Bergholz, Olga Taratuta, Pagu, Paulette Brupbacher, Qiu Jin, Rashid Jahan, Rosa Luxemburg, Salvadora Medina Onrubia, Sara Berenguer, Simone Weil, Sophia Perovskaya, Teresa Wilms Montt, Véra Figner, Véra Zassoulitch, Virgilia D'Andrea, Virginia Bolten, Voltairine de Cleyre, Yuriko Miyamoto.
